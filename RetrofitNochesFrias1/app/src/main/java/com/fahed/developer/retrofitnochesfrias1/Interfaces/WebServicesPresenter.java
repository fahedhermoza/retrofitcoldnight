package com.fahed.developer.retrofitnochesfrias1.Interfaces;

/**
 * Created by mac on 13/01/18.
 */

public interface WebServicesPresenter {
    void serviceAsynchronous();
    void serviceGet();
    void servicePost();
    void servicePostTagQuery();
    void servicePostTagField();
    void servicePostAuth();
    void servicePostTagBody();
    void servicePostTagBodyOverrideHeader();
    void servicePostTagBodyTagHeader();
    void serviceContainer();
}
