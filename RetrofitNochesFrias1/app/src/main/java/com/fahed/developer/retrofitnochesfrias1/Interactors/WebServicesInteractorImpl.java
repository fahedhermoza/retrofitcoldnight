package com.fahed.developer.retrofitnochesfrias1.Interactors;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.fahed.developer.retrofitnochesfrias1.Interfaces.OnWebServiceFinishListener;
import com.fahed.developer.retrofitnochesfrias1.Interfaces.WebServicesInteractor;
import com.fahed.developer.retrofitnochesfrias1.Models.Keyword;
import com.fahed.developer.retrofitnochesfrias1.Models.LoginUsuarioMovil;
import com.fahed.developer.retrofitnochesfrias1.Models.ResponseService;
import com.fahed.developer.retrofitnochesfrias1.Models.RespuestaLoginUsuarioMovil;
import com.fahed.developer.retrofitnochesfrias1.Models.SecurityLab;
import com.fahed.developer.retrofitnochesfrias1.Models.TiposContenedoresVentas;
import com.fahed.developer.retrofitnochesfrias1.Services.ServicesEndPoint;
import com.fahed.developer.retrofitnochesfrias1.Services.ServicesGenerator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * Created by mac on 13/01/18.
 */

public class WebServicesInteractorImpl implements WebServicesInteractor {
    final String url = "https://androidtutorials.herokuapp.com/";
    final String url2 = "http://devartpruebas.cloudapp.net:8080/ruedaNegocios/rest/";

    /*********************************** Synchronous
     * Sincrono, la invocación de un método o tarea se realiza, pero espera el resultado, y
     * no continua la ejecución hasta que se obtenga un resultado
     * **********************************/

    @Override
    public void webServiceAsynchronous(final OnWebServiceFinishListener listener) {
        new Peticion(listener).execute();
    }


    public static class Peticion extends AsyncTask<Void,Void,Void> {
        private OnWebServiceFinishListener listener;

        public Peticion(OnWebServiceFinishListener listener) {
            this.listener = listener;
        }

        @Override
        protected Void doInBackground(Void... params) {
            final String url = "https://androidtutorials.herokuapp.com/";

            Retrofit retrofit =  new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);

            //Get
            Call<List<ResponseService>> response = service.getUsersGet();
            //Post
            //Call<List<ResponseService>> response =service.getUsersPost();

            try {
                if(response.execute().isSuccessful()) {
                    for (ResponseService user : response.execute().body()) {
                        if (user != null)
                            Log.e("Respuesta:  ", user.getName() + " " + user.getLastName());
                    }
                    listener.operationSucces();
                }
            } catch (IOException e) {
                e.printStackTrace();
                listener.operationError();
            }
            return null;
        }
    }


    /*************************** Asynchronous
     * Asíncrono, se refiere a que realizas la invocación de un método o tarea pero continuas
     * con la ejecución sin esperar un resultado. Importante hacer notar que generalmente se
     * define un "Callback" que es un método que recibirá el resultado de la respuesta.
     * *************************************/

    @Override
    public void webServiceGet(final OnWebServiceFinishListener listener) {
        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);

        Call<List<ResponseService>> call =service.getUsersGet();
        call.enqueue(new Callback<List<ResponseService>>() {
            @Override
            public void onResponse(Call<List<ResponseService>> call, Response<List<ResponseService>> response) {
                for(ResponseService user: response.body()) {
                    Log.e("Respuesta ", "Id: " + user.getId());
                    Log.e("Respuesta ", "NickName" + user.getNickName());
                }
                listener.operationSucces();

            }

            @Override
            public void onFailure(Call<List<ResponseService>> call, Throwable t) {
                Log.e("onFailure "," Error");
                listener.operationError();
            }
        });
    }

    @Override
    public void webServicePost(final OnWebServiceFinishListener listener) {
        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);

        Call<List<ResponseService>> call =service.getUsersPost();
        call.enqueue(new Callback<List<ResponseService>>() {
            @Override
            public void onResponse(Call<List<ResponseService>> call, Response<List<ResponseService>> response) {
                for(ResponseService user: response.body()) {
                    Log.e("Respuesta ", "Id: " + user.getId());
                    Log.e("Respuesta ", "NickName" + user.getNickName());
                }
                listener.operationSucces();

            }

            @Override
            public void onFailure(Call<List<ResponseService>> call, Throwable t) {
                Log.e("onFailure "," Error");
                listener.operationError();
            }
        });
    }

    @Override
    public void webServicePostTagQuery(final OnWebServiceFinishListener listener) {
        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);

        Call<ResponseService> call =service.findUsersGet(3);
        call.enqueue(new Callback<ResponseService>() {
            @Override
            public void onResponse(Call<ResponseService> call, Response<ResponseService> response) {
                Log.e("Respuesta ", "Id: "+response.body().getId());
                Log.e("Respuesta ", "Nick: "+response.body().getNickName());
                listener.operationSucces();
            }

            @Override
            public void onFailure(Call<ResponseService> call, Throwable t) {
                Log.e("Error ", t.toString());
                listener.operationError();
            }
        });
    }

    @Override
    public void webServicePostTagField(final OnWebServiceFinishListener listener) {
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);

        Call<String> call =service.findUsersPost("FahedHermoza-Developer");

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("Respuesta ", response.body().toString());
                listener.operationSucces();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("Error ", t.toString());
                listener.operationError();
            }
        });
    }

    @Override
    public void webServicePostAuth(final OnWebServiceFinishListener listener) {
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);

        String auth = "Basic " + Base64.encodeToString(String.format("%s:%s", "tutoriales", "hackro").getBytes(), Base64.NO_WRAP);
        Log.e("+++++ ",auth);

        Call<String> call = service.auth(auth);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("Respuesta ", response.body().toString());
                listener.operationSucces();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("Error ", t.toString());
                listener.operationError();
            }
        });
    }

    @Override
    public void webServicePostTagBody(final OnWebServiceFinishListener listener) {
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);

        Call<ResponseService> call = service.body(new ResponseService(22, "Fahed", "Hermoza","DeHaF" ));

        call.enqueue(new Callback<ResponseService>() {
            @Override
            public void onResponse(Call<ResponseService> call, Response<ResponseService> response) {
                Log.e("Respuesta ", response.body().getName());
                listener.operationSucces();
            }

            @Override
            public void onFailure(Call<ResponseService> call, Throwable t) {
                listener.operationError();
            }
        });
    }

    @Override
    public void webServicePostTagBodyOverrideHeader(final OnWebServiceFinishListener listener) {
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl(url2)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);

        Call<RespuestaLoginUsuarioMovil> call= service.bodyUserMobile(new LoginUsuarioMovil("123456789","hermoza@peru.com" ,"foto.desnudo.com", "Manuela"));

        call.enqueue(new Callback<RespuestaLoginUsuarioMovil>() {
            @Override
            public void onResponse(Call<RespuestaLoginUsuarioMovil> call, Response<RespuestaLoginUsuarioMovil> response) {
                if(response.isSuccessful()) {
                    Log.e("Respuesta ", response.body().getRespuesta());
                    Log.e("Token ", response.body().getToken());
                    //Actualizar Security Lab
                    SecurityLab.get().setKey(response.body().getToken());

                    listener.getContainer();

                    //listener.operationSucces();
                }else{
                    Log.e("onFailure ", "Inicio de sesión incorrecto");
                    listener.operationError();
                }
            }

            @Override
            public void onFailure(Call<RespuestaLoginUsuarioMovil> call, Throwable t) {
                Log.e("onFailure ", t.toString());
                listener.operationError();
            }
        });
    }

    @Override
    public void webServicePostTagBodyTagHeader(final OnWebServiceFinishListener listener) {
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl(url2)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);

        Call<RespuestaLoginUsuarioMovil> call= service.bodyHeaderUserMobile("application/json",new LoginUsuarioMovil("123456789","hermoza@peru.com" ,"foto.desnudo.com", "Manuela"));

        call.enqueue(new Callback<RespuestaLoginUsuarioMovil>() {
            @Override
            public void onResponse(Call<RespuestaLoginUsuarioMovil> call, Response<RespuestaLoginUsuarioMovil> response) {
                if(response.isSuccessful()) {
                    Log.e("Respuesta ", response.body().getRespuesta());
                    Log.e("Token ", response.body().getToken());
                    listener.operationSucces();
                }else{
                    Log.e("onFailure ", "Inicio de sesión incorrecto");
                    listener.operationError();
                }
            }

            @Override
            public void onFailure(Call<RespuestaLoginUsuarioMovil> call, Throwable t) {
                Log.e("onFailure ", t.toString());
                listener.operationError();
            }
        });
    }

    @Override
    public void webServicePostFindContainers(final OnWebServiceFinishListener listener,
                                             final String token,
                                             final String keyword) {
        /*Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl(url2)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build(); */

        //ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);
        //Call<List<TiposContenedoresVentas>> call =service.findContainers("Bearer "+token,new Keyword(keyword));

        ServicesEndPoint service = ServicesGenerator.createService(ServicesEndPoint.class,token);
        Call<List<TiposContenedoresVentas>> call = service.findContainers2(new Keyword(keyword));

        call.enqueue(new Callback<List<TiposContenedoresVentas>>() {
            @Override
            public void onResponse(Call<List<TiposContenedoresVentas>> call, Response<List<TiposContenedoresVentas>> response) {
                if(response.isSuccessful()) {
                    for (TiposContenedoresVentas contenedor : response.body()) {
                        Log.e("Nombre ", contenedor.getNombre());
                        Log.e("IdTipo ", contenedor.getIdtipo()+"");
                        Log.e("UrlImagen", contenedor.getUrlImagen());

                    }
                    listener.operationSucces();
                }else{
                    listener.operationError();
                }
            }

            @Override
            public void onFailure(Call<List<TiposContenedoresVentas>> call, Throwable t) {
                Log.e("Error ", t.toString());
                listener.operationError();
            }
        });

    }
}
