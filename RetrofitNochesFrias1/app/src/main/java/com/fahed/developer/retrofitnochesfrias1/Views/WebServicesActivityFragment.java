package com.fahed.developer.retrofitnochesfrias1.Views;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.fahed.developer.retrofitnochesfrias1.Interfaces.WebServicesPresenter;
import com.fahed.developer.retrofitnochesfrias1.Interfaces.WebServicesView;
import com.fahed.developer.retrofitnochesfrias1.Presenters.WebServicesPresenterImpl;
import com.fahed.developer.retrofitnochesfrias1.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class WebServicesActivityFragment extends Fragment implements WebServicesView,View.OnClickListener{
    private View viewRoot;
    private WebServicesPresenter presenter;
    private Button buttonRequestGet;
    private Button buttonRequestPost;
    private Button buttonRequestPostTagQuery;
    private Button buttonRequestPostField;
    private Button buttonRequestPostAuthentication;
    private Button buttonRequestPostTagBody;
    private Button buttonRequestPostTagBodyOverrideHeader;
    private Button buttonRequestPostTagBodyTagHeader;


    public WebServicesActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewRoot = inflater.inflate(R.layout.fragment_web_services, container, false);
        buttonRequestGet = (Button) viewRoot.findViewById(R.id.buttonRequestGet);
        buttonRequestPost = (Button) viewRoot.findViewById(R.id.buttonRequestPost);
        buttonRequestPostTagQuery = (Button) viewRoot.findViewById(R.id.buttonRequestPostTagQuery);
        buttonRequestPostField = (Button) viewRoot.findViewById(R.id.buttonRequestPostTagField);
        buttonRequestPostAuthentication = (Button) viewRoot.findViewById(R.id.buttonRequestPostAuthentication);
        buttonRequestPostTagBody = (Button) viewRoot.findViewById(R.id.buttonRequestPostTagBody);
        buttonRequestPostTagBodyOverrideHeader = (Button) viewRoot.findViewById(R.id.buttonRequestPostTagBodyOverrideHeader);
        buttonRequestPostTagBodyTagHeader = (Button) viewRoot.findViewById(R.id.buttonRequestPostTagBodyTagHeader);


        buttonRequestGet.setOnClickListener(this);
        buttonRequestPost.setOnClickListener(this);
        buttonRequestPostTagQuery.setOnClickListener(this);
        buttonRequestPostField.setOnClickListener(this);
        buttonRequestPostAuthentication.setOnClickListener(this);
        buttonRequestPostTagBody.setOnClickListener(this);

        presenter =  new WebServicesPresenterImpl(this);
        return viewRoot;
    }


    @Override
    public void setSuccess() {Snackbar.make(viewRoot, "Servicio Exito", Snackbar.LENGTH_LONG).show();}

    @Override
    public void setError() {
        Snackbar.make(viewRoot, "Hubo un error", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonRequestGet:
                presenter.serviceGet();
                break;
            case R.id.buttonRequestPost:
                presenter.servicePost();
                break;
            case R.id.buttonRequestPostTagQuery:
                presenter.servicePostTagQuery();
                break;
            case R.id.buttonRequestPostTagField:
                presenter.servicePostTagField();
                break;
            case R.id.buttonRequestPostAuthentication:
                presenter.servicePostAuth();
                break;
            case R.id.buttonRequestPostTagBody:
                presenter.servicePostTagBody();
                break;
            case R.id.buttonRequestPostTagBodyOverrideHeader:
                presenter.servicePostTagBodyOverrideHeader();
                break;
            case R.id.buttonRequestPostTagBodyTagHeader:
                presenter.servicePostTagBodyTagHeader();
                break;
            default:
                break;
        }
    }
}
