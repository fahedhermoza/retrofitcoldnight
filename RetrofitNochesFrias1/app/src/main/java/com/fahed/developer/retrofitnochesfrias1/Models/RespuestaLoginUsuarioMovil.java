package com.fahed.developer.retrofitnochesfrias1.Models;

/**
 * Created by mac on 7/01/18.
 */

public class RespuestaLoginUsuarioMovil {

    private String respuesta;
    private String token;

    /**
     * No args constructor for use in serialization
     *
     */
    public RespuestaLoginUsuarioMovil() {
    }

    /**
     *
     * @param token
     * @param respuesta
     */
    public RespuestaLoginUsuarioMovil(String respuesta, String token) {
        super();
        this.respuesta = respuesta;
        this.token = token;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}