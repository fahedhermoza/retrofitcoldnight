package com.fahed.developer.retrofitnochesfrias1.Models;

/**
 * Created by mac on 14/01/18.
 */

public class Keyword {

    private String keyword;

    /**
     * No args constructor for use in serialization
     *
     */
    public Keyword() {
    }

    /**
     *
     * @param keyword
     */
    public Keyword(String keyword) {
        super();
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

}
