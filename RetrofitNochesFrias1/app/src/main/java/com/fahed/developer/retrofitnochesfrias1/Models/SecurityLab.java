package com.fahed.developer.retrofitnochesfrias1.Models;

/**
 * Created by mac on 22/01/18.
 */

public class SecurityLab {
    private static SecurityLab sSecurityLab;
    private String key;

    public static SecurityLab get(){
        if(sSecurityLab == null){
            sSecurityLab = new SecurityLab();
        }
        return sSecurityLab;
    }

    private SecurityLab(){
        key = null;
    }

    public String getKey(){
        return key;
    }

    public void setKey(String key){
        this.key = key;
    }

}
