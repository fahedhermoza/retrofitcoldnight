package com.fahed.developer.retrofitnochesfrias1.Models;

/**
 * Created by mac on 6/01/18.
 */

public class LoginUsuarioMovil {

    private String uid;
    private String email;
    private String urlFoto;
    private String nombres;

    /**
     * No args constructor for use in serialization
     *
     */
    public LoginUsuarioMovil() {
    }

    /**
     *
     * @param uid
     * @param nombres
     * @param email
     * @param urlFoto
     */
    public LoginUsuarioMovil(String uid, String email, String urlFoto, String nombres) {
        super();
        this.uid = uid;
        this.email = email;
        this.urlFoto = urlFoto;
        this.nombres = nombres;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

}