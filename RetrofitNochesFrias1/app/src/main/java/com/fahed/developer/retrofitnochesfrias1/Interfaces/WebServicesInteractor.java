package com.fahed.developer.retrofitnochesfrias1.Interfaces;

/**
 * Created by mac on 13/01/18.
 */

public interface WebServicesInteractor {
    void webServiceAsynchronous(final OnWebServiceFinishListener listener);
    void webServiceGet(final OnWebServiceFinishListener listener);
    void webServicePost(final OnWebServiceFinishListener listener);
    void webServicePostTagQuery(final OnWebServiceFinishListener listener);
    void webServicePostTagField(final OnWebServiceFinishListener listener);
    void webServicePostAuth(final OnWebServiceFinishListener listener);
    void webServicePostTagBody(final OnWebServiceFinishListener listener);
    void webServicePostTagBodyOverrideHeader(final OnWebServiceFinishListener listener);
    void webServicePostTagBodyTagHeader(final OnWebServiceFinishListener listener);
    //
    void webServicePostFindContainers(final OnWebServiceFinishListener listener,final String token,
                                      final String keyword);
}
