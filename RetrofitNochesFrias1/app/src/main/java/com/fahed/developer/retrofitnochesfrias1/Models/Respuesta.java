package com.fahed.developer.retrofitnochesfrias1.Models;

/**
 * Created by mac on 18/01/18.
 */

public class Respuesta {

    private String respuesta;

    /**
     * No args constructor for use in serialization
     *
     */
    public Respuesta() {
    }

    /**
     *
     * @param respuesta
     */
    public Respuesta(String respuesta) {
        super();
        this.respuesta = respuesta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

}
