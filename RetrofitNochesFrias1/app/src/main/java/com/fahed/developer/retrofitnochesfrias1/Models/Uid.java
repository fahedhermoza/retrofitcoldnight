package com.fahed.developer.retrofitnochesfrias1.Models;

/**
 * Created by mac on 18/01/18.
 */

public class Uid {

    private String uid;

    /**
     * No args constructor for use in serialization
     *
     */
    public Uid() {
    }

    /**
     *
     * @param uid
     */
    public Uid(String uid) {
        super();
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

}
