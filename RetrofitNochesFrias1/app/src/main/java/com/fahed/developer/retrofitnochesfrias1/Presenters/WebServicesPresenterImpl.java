package com.fahed.developer.retrofitnochesfrias1.Presenters;

import com.fahed.developer.retrofitnochesfrias1.Interactors.WebServicesInteractorImpl;
import com.fahed.developer.retrofitnochesfrias1.Interfaces.OnWebServiceFinishListener;
import com.fahed.developer.retrofitnochesfrias1.Interfaces.WebServicesInteractor;
import com.fahed.developer.retrofitnochesfrias1.Interfaces.WebServicesPresenter;
import com.fahed.developer.retrofitnochesfrias1.Interfaces.WebServicesView;
import com.fahed.developer.retrofitnochesfrias1.Models.SecurityLab;

/**
 * Created by mac on 13/01/18.
 */

public class WebServicesPresenterImpl implements WebServicesPresenter,OnWebServiceFinishListener {
    private WebServicesView view;
    private WebServicesInteractor interactor;

    public WebServicesPresenterImpl(WebServicesView view) {
        this.view = view;
        interactor = new WebServicesInteractorImpl();
        //this.servicePostBodyHeader();
        //this.servicePostBodyInHeader();
    }

    @Override
    public void serviceAsynchronous() {
        if(view != null){
            interactor.webServiceAsynchronous(this);
        }
    }

    @Override
    public void serviceGet() {
        if(view != null){
            interactor.webServiceGet(this);
        }
    }

    @Override
    public void servicePost() {
        if(view != null){
            interactor.webServicePost(this);
        }
    }

    @Override
    public void servicePostTagQuery() {
        if(view != null){
            interactor.webServicePostTagQuery(this);
        }
    }

    @Override
    public void servicePostTagField() {
        if(view != null){
            interactor.webServicePostTagField(this);
        }
    }

    @Override
    public void servicePostAuth() {
        if(view != null){
            interactor.webServicePostAuth(this);
        }
    }

    @Override
    public void servicePostTagBody() {
        if(view != null){
            interactor.webServicePostTagBody(this);
        }
    }

    @Override
    public void servicePostTagBodyOverrideHeader() {
        if(view != null){
            interactor.webServicePostTagBodyOverrideHeader(this);
        }
    }

    @Override
    public void servicePostTagBodyTagHeader() {
        if(view != null){
            interactor.webServicePostTagBodyTagHeader(this);
        }
    }
    //
    @Override
    public void serviceContainer() {
        interactor.webServicePostFindContainers(this,SecurityLab.get().getKey(),"Inmueble");
    }

    @Override
    public void operationSucces() {
        view.setSuccess();
    }

    @Override
    public void operationError() {
        view.setError();
    }

    @Override
    public void getContainer() {
        interactor.webServicePostFindContainers(this,SecurityLab.get().getKey(),"Inmueble");
    }
}
