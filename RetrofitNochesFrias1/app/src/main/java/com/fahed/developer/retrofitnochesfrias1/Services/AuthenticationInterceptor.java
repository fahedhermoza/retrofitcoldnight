package com.fahed.developer.retrofitnochesfrias1.Services;

import android.os.AsyncTask;
import android.util.Log;

import com.fahed.developer.retrofitnochesfrias1.Models.Respuesta;
import com.fahed.developer.retrofitnochesfrias1.Models.SecurityLab;
import com.fahed.developer.retrofitnochesfrias1.Models.Uid;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mac on 15/01/18.
 */

public class AuthenticationInterceptor implements Interceptor {

    private static String authToken;

    public AuthenticationInterceptor(String token) {
        this.authToken = token;
    }

    @Override
    public Response intercept(final Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder builder = original.newBuilder()
                .header("Authorization", "Bearer " + authToken);

        Request request = builder.build();

        // try the request
        Response response = chain.proceed(request);
        Log.e("Codigo", "" + response.code());
        if (response.code() == 401) {
            // get a new token (I use a synchronous Retrofit call)
            Gson gson = new GsonBuilder().setLenient().create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ServicesGenerator.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ServicesEndPoint service = retrofit.create(ServicesEndPoint.class);

            Call<Respuesta> callResponse = service.updateToken(new Uid("123456789"));
            Request newRequest=null;

            try {

                if (callResponse.clone().execute().isSuccessful()) {
                    String data = callResponse.execute().body().getRespuesta();
                    if(!data.equals("Error")){
                        //caso cuando devuelve Error y token
                        authToken = data;
                        SecurityLab.get().setKey(authToken);

                        Request.Builder newBuilder = original.newBuilder()
                                .header("Authorization", "Bearer " + authToken);
                        newRequest = newBuilder.build();

                        Log.e("Token  ", authToken);
                    }else{
                        Log.e("Error ", "Contenido de token invalido ");
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Error ", e.toString());
                return null;
            }
            return chain.proceed(newRequest);


        } else {
            return chain.proceed(request);
        }

    }


}