package com.fahed.developer.retrofitnochesfrias1.Services;

import com.fahed.developer.retrofitnochesfrias1.Models.Keyword;
import com.fahed.developer.retrofitnochesfrias1.Models.LoginUsuarioMovil;
import com.fahed.developer.retrofitnochesfrias1.Models.ResponseService;
import com.fahed.developer.retrofitnochesfrias1.Models.Respuesta;
import com.fahed.developer.retrofitnochesfrias1.Models.RespuestaLoginUsuarioMovil;
import com.fahed.developer.retrofitnochesfrias1.Models.TiposContenedoresVentas;
import com.fahed.developer.retrofitnochesfrias1.Models.Uid;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by mac on 3/01/18.
 */

public interface ServicesEndPoint {
    @GET("usersFake")
    Call<List<ResponseService>> getUsersGet();

    @POST("usersFake")
    Call<List<ResponseService>> getUsersPost();

    //https://androidtutorials.herokuapp.com/findUser?id=1
    @GET("findUser")
    Call<ResponseService> findUsersGet(@Query("id") int idUser);

    //Con parametros
    @FormUrlEncoded
    @POST("findUserPost")
    Call<String> findUsersPost(@Field("name") String nombre);

    //Con auth
    @POST("auth")
    Call<String> auth(@Header("authorization") String auth);

    @POST("returnObject")
    Call<ResponseService> body(@Body ResponseService body);

    /*** Metodo 1 ***/
    //@Headers("Content-Type: application/json; charset=utf-8")
    @Headers("Content-Type: application/json")
    @POST("usuarioMovil/login")
    Call<RespuestaLoginUsuarioMovil> bodyUserMobile(@Body LoginUsuarioMovil body);


    /*** Metodo 2 ***/
    @POST("usuarioMovil/login")
    Call<RespuestaLoginUsuarioMovil> bodyHeaderUserMobile(@Header("Content-Type") String content_type,
                                                      @Body LoginUsuarioMovil body);
    //@Headers({"Content-Type: application/json"})
    @FormUrlEncoded
    @POST("usuarioMovil/login")
    Call<RespuestaLoginUsuarioMovil> findUsuarioMovil(@Field("idFacebook") String idfacebook,
                                      @Field("email") String email,
                                      @Field("urlFoto") String urlFoto,
                                      @Field("nombres")String nombres,
                                      @Field("apellidos")String apellidos);

    //Con auth
    @Headers("Content-Type: application/json")
    @POST("tipo/obtenerTipos")
    Call<List<TiposContenedoresVentas>> findContainers(@Header("Authorization") String authToken,
                                                       @Body Keyword keyword);

    @Headers("Content-Type: application/json")
    @POST("tipo/obtenerTipos")
    Call<List<TiposContenedoresVentas>> findContainers2(@Body Keyword keyword);

    @Headers("Content-Type: application/json")
    @POST("usuarioMovil/actualizarlogin")
    Call<Respuesta> updateToken(@Body Uid uid);


}
