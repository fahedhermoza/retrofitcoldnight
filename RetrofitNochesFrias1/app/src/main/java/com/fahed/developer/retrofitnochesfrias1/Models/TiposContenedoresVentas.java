package com.fahed.developer.retrofitnochesfrias1.Models;

/**
 * Created by mac on 14/01/18.
 */

public class TiposContenedoresVentas {

    private int idtipo;
    private String urlImagen;
    private String nombre;
    private boolean estado;

    public TiposContenedoresVentas() {
    }

    public TiposContenedoresVentas(int idtipo, String urlImagen, String nombre, boolean estado) {
        super();
        this.idtipo = idtipo;
        this.urlImagen = urlImagen;
        this.nombre = nombre;
        this.estado = estado;
    }

    public int getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(int idtipo) {
        this.idtipo = idtipo;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

}