package com.fahed.developer.retrofitnochesfrias1.Interfaces;

/**
 * Created by mac on 13/01/18.
 */

public interface OnWebServiceFinishListener {

    void operationSucces();

    void operationError();

    void getContainer();
}
