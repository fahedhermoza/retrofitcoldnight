# Retrofit Cold Night

[![forthebadge](https://forthebadge.com/images/badges/makes-people-smile.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/winter-is-coming.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/built-for-android.svg)](https://forthebadge.com) 

## Descripción
Ejemplos de consumo de servicios REST con la API [RETROFIT 2](https://square.github.io/retrofit/), con Authentication(OAuth 2.0) y Singleton.

## Características
- Consumir servicio GET.
- Consumir servicio POST.
- Consumir servicio POST TAG QUERY.
- Consumir servicio POST FIELD.
- Consumir servicio POST AUTHENTIFICATION.
- Consumir servicio POST TAG BODY.
- Consumir servicio POST TAG BODY OVERRIDE HEADER.
- Consumir servicio POST TAG BODY, HEADER.
